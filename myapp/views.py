from sqlalchemy import func, text
from .app import app, db
from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for, flash
from .models import User, Author, Music, Link, Genre, Playlist, get_genre_by_genre, get_genre_by_name
from .models import get_sample, get_author, get_details, get_author_by_name, get_music_by_author, get_recherche, \
    get_liste_author, get_genre, get_music_by_author, get_music_by_id, recherche_annee, recherche_album, \
    recherche_artiste, recherche_genre, get_playlist_by_user, get_playlist, recuperer_note, get_playlist_by_name_user,get_playlist_by_id, get_if_like,get_like,get_music_by_genre


from flask_wtf import FlaskForm
from wtforms import Form
from wtforms import PasswordField
from wtforms import StringField, HiddenField, SubmitField
from wtforms.validators import DataRequired, Length, ValidationError, Email, EqualTo
from hashlib import sha256
from flask_login import login_user, current_user, logout_user, login_required
from myapp.models import Note


@app.route("/home/")
@login_required
def home():
    user = current_user.username
    playlist_like = get_playlist_by_name_user("Like", user)
    liste = []
    for x in range(len(playlist_like)):
        liste.append(playlist_like[x].musicId)
    return render_template(
        "home.html",
        title="Spotizeer",
        musics=get_sample(),
        playlist=liste
    )

ROWS_PER_PAGE = 12
@app.route("/home/test")
@login_required
def hometest():
    user = current_user.username
    playlist_like = get_playlist_by_name_user("Like", user)
    liste = []
    for x in range(len(playlist_like)):
        liste.append(playlist_like[x].musicId)

    page = request.args.get('page', 1, type=int)
    music = Music.query.paginate(page=page,per_page=ROWS_PER_PAGE)
    return render_template(
        "hometest.html",
        title="Spotizeer",
        musics=music,
        playlist=liste
    )

# ===============================================================User===============================================================
class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


@app.route("/login/", methods=("GET", "POST",))
def login():
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            return redirect(url_for("home"))
    return render_template(
        "login.html",
        form=f, title="login")


@app.route("/logout/")
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


class RegistrationForm(FlaskForm):
    """Classe de formulaire inscription"""
    pseudo = StringField('Pseudo', validators=[DataRequired(), Length(min=2, max=32)])
    mot_de_passe = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=6, max=32)])
    confirm_mot_de_passe = PasswordField('Confirmer mot de passe',
                                         validators=[DataRequired(), Length(min=6, max=32), EqualTo("mot_de_passe")])
    submit = SubmitField("S'inscrire")
    next = HiddenField()

    def validate_pseudo(self, pseudo):
        pseudo_utilisateur = User.query.get(pseudo.data)
        if pseudo_utilisateur:
            raise ValidationError("Ce pseudonyme est déjà utilisé")
        return True


# ===============================================================Recherche===============================================================

# @app.route('/search', methods=["GET"])
# def search():
#     return render_template(
#         "search.html",
#         recherche=get_recherche(request.args.get('recherche')),
#         title="Search : " + str(request.args.get('recherche'))
#     )
@app.route("/home/search/", methods=['POST', 'GET'])
@login_required
def search():
    recherche = ""
    liste_album = []
    type_recherche = ""
    if request.method == "POST":
        recherche = request.form['recherche']
        type_recherche = request.form.get('type')

        if type_recherche == "annee":
            liste_album = recherche_annee(recherche)
        elif type_recherche == "album":
            liste_album = recherche_album(recherche)
        elif type_recherche == "artiste":
            liste_album = recherche_artiste(recherche)
        elif type_recherche == "genre":
            liste_album = recherche_genre(recherche)
    return render_template("search.html", type_recherche=type_recherche, recherche=recherche, la_liste=liste_album,
                           title="search")


# ===============================================================Auteur===============================================================

class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])


@app.route('/home/add/author', methods=('GET', 'POST'))
@login_required
def create_author():
    if request.method == 'POST':
        qry = db.session.query(func.max(Author.authorId))
        last = qry.first()[0]
        author_id = last + 1
        name = request.form['author']
        if not name:
            flash('Name is required!')
        else:
            if get_author_by_name(name) is None:
                db.session.add(Author(authorId=author_id, name=name))
                db.session.commit()
                return redirect(url_for('home'))
            else:
                flash('Author already in the DataBase')

    return render_template('add.html', title="Add an author")


@app.route("/home/edit/author/<int:id>", methods=['GET', 'POST'])
@login_required
def edit_author(id=None):
    nom = None
    if id is not None:
        a = get_author(id)
        nom = a.name
    else:
        a = None
    f = AuthorForm(id=id, name=nom)
    if request.method == 'POST':
        db.session.delete(Author.query.filter_by(authorId=f.id.data).first())
        db.session.commit()
        return redirect(url_for('home'))
    return render_template("edit-author.html", author=a, form=f, title="Edit an author")


@app.route("/home/save/author/", methods=["POST"])
@login_required
def save_author():
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        if not get_author_by_name(f.name.data):
            if f.id.data != "":
                id = int(f.id.data)
                a = get_author(id)
                a.name = f.name.data
            else:
                a = Author(name=f.name.data)
                db.session.add(a)
            db.session.commit()
            # id = a.id
            id = int(f.id.data)
            return redirect(url_for('one_author', id=id))
        else:
            flash("Author already exists")
    return render_template("edit-author.html", author=a, form=f, title="save an author")


@app.route('/home/auteur/<int:id>')
@login_required
def one_author(id):
    return render_template('one_author.html',
                           author=get_author(id),
                           music=get_music_by_author(id),
                           title="auteur"
                           )


@app.route('/', methods=['GET', 'POST'])
def page_inscription():
    """inscription"""
    form = RegistrationForm()
    if form.validate_on_submit():
        mdp = sha256()
        mdp.update(form.mot_de_passe.data.encode())
        utilisateur = User(username=form.pseudo.data, password=mdp.hexdigest())
        db.session.add(utilisateur)
        db.session.commit()
        return redirect(url_for("login"))
    return render_template("register.html", title="Inscription", form=form)


# ===============================================================Musique===============================================================
@app.route("/home/detail/<id>")
@login_required
def detail(id):
    user = current_user.username
    return render_template("details.html",
                           music=get_details(id),
                           title="detail",
                           like=get_if_like(user,id),
                           note = get_like(user,id)
                           )


@app.route('/home/add/music', methods=('GET', 'POST'))
@login_required
def create_music():
    if request.method == 'POST':
        qry = db.session.query(func.max(Music.entryId))
        last = qry.first()[0]
        entry_id = last + 1
        release_year = request.form['release']
        title_music = request.form['title']
        parent_music = request.form['parent']
        img_music = request.form['img']
        author_id = db.session.query(Author.authorId).filter(Author.name == parent_music)
        genre_music = request.form['genre']
        youtube_api = request.form['youtube']
        if not title_music:
            flash('Title is required!')
        if not img_music:
            img_music = (
                "https://zdnet1.cbsistatic.com/hub/i/r/2020/05/11/703e8b53-6149-47dd-9969-f7de6b044b3e/resize/1200xauto/311e6c882043a19e7b68cef0de73456f/apple-music.png")
        if not youtube_api:
            flash('Please enter link of Youtube API for improve this web site :)')
        else:
            db.session.add(
                Music(entryId=entry_id, releaseYear=release_year, title=title_music, parent=parent_music, img=img_music,
                      youtube=youtube_api, author_id=author_id), Link(music_id=entry_id, genre_id=genre_music))
            db.session.commit()
            return redirect(url_for('home'))
    return render_template('addMusic.html', author=get_liste_author(), musics=get_sample(), genres=get_genre(),
                           title="add Music")


@app.route('/home/detail/<id>/delete', methods=('GET', 'POST'))
@login_required
def delete_music(id):
    music = get_details(id)
    if request.method == 'POST':
        db.session.delete(music)
        db.session.commit()
        return redirect(url_for('home'))


@app.route('/home/detail/<id>/edit', methods=('GET', 'POST'))
@login_required
def edit_music(id):
    music = get_details(id)
    if request.method == 'POST':
        release_year = request.form['release']
        title_music = request.form['title']
        img_music = request.form['img']
        youtube_api = request.form['youtube']
        current_music = get_details(id)
        current_music.releaseYear = release_year
        current_music.title = title_music
        current_music.img = img_music
        current_music.youtube = youtube_api
        db.session.commit()
        return redirect(url_for("detail",id=id))
    return render_template("edit-music.html",music=get_details(id),author=get_liste_author(), genres=get_genre())



# ===============================================================Playlist===============================================================


@app.route('/home/add/playlist', methods=('GET', 'POST'))
@login_required
def create_playlist():
    if request.method == 'POST':
        if get_playlist() == []:
            id = 1
        else:
            qry = db.session.query(func.max(Playlist.Id))
            last = qry.first()[0]
            id = last + 1
        user = current_user.username
        playlist_name = request.form['playlist-name']
        music = db.session.query(Music.entryId).filter(Music.title == request.form['music-choices'])
        if not playlist_name:
            flash("Name is required")
        else:
            db.session.add(Playlist(Id=id, username=user, playlistName=playlist_name, musicId=music))
            db.session.commit()
            return redirect(url_for('playlist', name=get_playlist_by_name_user(playlist_name,user)))
    return render_template('addPlaylist.html', musics=get_sample(), title="AddPlaylist")


@app.route('/home/<id>', methods=('GET', 'POST'))
@login_required
def add_to_playlist(id):
    if request.method == 'POST':
        playlist_name = "Like"
        user = current_user.username
        liste = []
        playlist_like = get_playlist_by_name_user("Like", user)
        for x in range(len(playlist_like)):
            liste.append(playlist_like[x].musicId)
        if Playlist.query.filter_by(username=user,playlistName=playlist_name, musicId=id).first() is None:
            if get_playlist() == []:
                id_playlist = 1
            else:
                qry = db.session.query(func.max(Playlist.Id))
                last = qry.first()[0]
                id_playlist = last + 1

            db.session.add(Playlist(Id=id_playlist, username=user, playlistName=playlist_name, musicId=id))
            db.session.commit()
            id_playlistI = db.session.query(Playlist.Id).filter(Playlist.username == user,
                                                               Playlist.playlistName == playlist_name,
                                                               Playlist.musicId == id).first()
            print(id_playlistI)
            print(id_playlistI[-1])

        else:
            playlist = Playlist.query.filter_by(username=user, musicId=id).first()
            id_playlist = playlist.Id
            playlist_supp=get_playlist_by_id(id_playlist)
            db.session.delete(playlist_supp)
            db.session.commit()
    return render_template(
        "home.html",
        title="Spotizeer",
        musics=get_sample(),
        playlist=liste
    )


@app.route('/home/<id>', methods=('GET', 'POST'))
@login_required
def delete_to_playlist(id):
    if request.method == 'POST':
        playlist_name = "Like"
        user = current_user.username
        id_playlist = db.session.query(Playlist.Id).filter(Playlist.username == user,
                                                           Playlist.playlistName == playlist_name,
                                                           Playlist.musicId == id)
        db.session.delete(Playlist(Id=id_playlist, username=user, playlistName=playlist_name, musicId=id))
        db.session.commit()
        return redirect(url_for('create_genre'))


@app.route("/home/playlist/")
@login_required
def playlist():
    liste = []
    for elem in db.session.query(Playlist).distinct(Playlist.playlistName):
        if elem.playlistName not in liste:
            liste.append(elem.playlistName)
    print(liste)#affiche toutes les playlist avec des noms différents
    user = current_user.username
    play = get_playlist_by_user(user)
    if play == []:
        return redirect(url_for('home'))

    return render_template("playlist.html",
                           play=play,
                           music=get_sample(),
                           liste=liste)


# ==============================================================Genre===================================================================

@app.route('/home/add/genre', methods=('GET', 'POST'))
@login_required
def create_genre():
    if request.method == 'POST':
        genre = request.form['genre']
        if not genre:
            flash('Name is required!')
        else:
            if get_genre_by_genre(genre) is None:
                db.session.add(Genre(genre=genre))
                db.session.commit()
                return redirect(url_for('home'))
            else:
                flash('Genre already in the DataBase')

    return render_template('addGenre.html', title="AddGenre")


# ==============================================================Notes===================================================================
@app.route('/home/detail/<id>', methods=('GET', 'POST'))
@login_required
def add_note(id):
    if request.method == 'POST':
        note = int(request.form['rating'])
        user = current_user.username
        musique = id
        if db.session.query(Note).filter(Note.musicId == musique, Note.username == user).first() is not None:
            current_note = db.session.query(Note).filter(Note.musicId == musique, Note.username == user).first()
            current_note.note = note
            db.session.commit()
            return redirect(url_for('detail', id=id))
        else:
            db.session.add(Note(musicId=musique, note=note, username=user))
            db.session.commit()
            return redirect(url_for('detail', id=id))
    return render_template('details.html', music=get_details(id), title="detail")


# ==============================================================Genre===================================================================

@login_required
@app.route('/home/edit/genre', methods=('GET', 'POST'))
def edit_genre():
    if request.method == 'POST':
        genre = request.form.get('newGenre')

        le_genre = request.form['genre']
        print(le_genre)
        if not genre:
            flash('Name is required!')
        else:
            current_genre = get_genre_by_name(le_genre)
            current_genre.genre = genre
            db.session.commit()
            return redirect(url_for('home'))
    return render_template('edit-genre.html', genres=get_genre(), title="edit genre")


@login_required
@app.route('/home/edit/genre/delete', methods=('GET', 'POST'))
def delete_genre():
    if request.method == 'POST':
        le_genre = request.form['genre']
        current_genre = get_genre_by_name(le_genre)
        print(current_genre) 
        db.session.delete(current_genre)
        db.session.commit()
        return redirect(url_for('home'))
    return render_template('edit-genre.html', genres=get_genre())
