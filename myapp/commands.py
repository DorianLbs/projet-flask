# coding: utf-8
import click
from .app import app, db


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    '''Creates the tables and populates them with data'''
    # création de toutes les tables
    db.create_all()

    # chargement de notre jeu de données
    import yaml
    musics = yaml.load(open(filename), Loader=yaml.FullLoader)

    # import des modèles
    from .models import Author, Music, Genre, Link

    #création de tous les auteurs
    authors = {}
    for b in musics:
        a = b["by"]
        if a not in authors:
            o = Author(name=a)
            db.session.add(o)
            authors[a] = o
    db.session.commit()

    #création de tous les genres
    genres = {}
    for b in musics:
        if len(b["genre"]) != 1:
            for x in b["genre"]:
                if x not in genres:
                    a = Genre(genre=x)
                    db.session.add(a)
                    genres[x] = a 
        else : 
            if x not in genres:
                a = Genre(genre=x)
                db.session.add(a)
                genres[a] = o 
    db.session.commit()

    for b in musics:
        if len(b["genre"]) == 0:
            pass
        if len(b["genre"]) != 1:
            for x in b["genre"]:
                o = Link(music_id=b['entryId'],
                genre_id=x)
                db.session.add(o)
        else : 
            for x in b["genre"]:
                o = Link(music_id=b['entryId'],
                        genre_id=x)
                db.session.add(o)
    db.session.commit()       


    # création de toutes les musiques
    for b in musics:
        a = authors[b["by"]]
        o = Music(entryId=b['entryId'],
                  releaseYear=b["releaseYear"],
                  title=b["title"],
                  parent=b["parent"],
                  img=b["img"],
                  youtube=b["youtube"],
                  author_id=a.authorId)
        db.session.add(o)
    db.session.commit()


@app.cli.command()
def syncdb():
    '''met à jour la bd'''
    db.create_all()


@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    '''Adds a new user.'''
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username=username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()

# pour ajouter un user en ligne de cmd : flask newuser user password
# pour modifier un mdp: flask passwd user newpassword
