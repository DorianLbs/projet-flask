from .app import db
from flask_login import UserMixin
from .app import login_manager


class Author(db.Model):
    authorId = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Author (%d) %s>" % (self.authorId, self.name)


class Music(db.Model):
    entryId = db.Column(db.Integer, primary_key=True)
    releaseYear = db.Column(db.Integer)
    title = db.Column(db.String(50))
    parent = db.Column(db.String(100))
    img = db.Column(db.String(100))
    genre_music = db.relationship("Genre", secondary="link")
    youtube = db.Column(db.String(1000))
    author_id = db.Column(db.Integer, db.ForeignKey("author.authorId"))
    author = db.relationship("Author", backref=db.backref("books", lazy="dynamic"))

    def __repr__(self):
        return "<Book (%d) %s>" % (self.entryId, self.title)


class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))
    note_user = db.relationship("Note")

    def get_id(self):
        return self.username


class Genre(db.Model):
    genre = db.Column(db.String(50), primary_key=True)
    music_genre_id = db.relationship("Music", secondary="link")


class Note(db.Model):
    musicId = db.Column(db.Integer, db.ForeignKey('music.entryId'), primary_key=True)
    note = db.Column(db.Integer)
    username = db.Column(db.String(50), db.ForeignKey('user.username'), primary_key=True)


class Link(db.Model):
    music_id = db.Column(db.Integer, db.ForeignKey('music.entryId'), primary_key=True)
    genre_id = db.Column(db.String(50), db.ForeignKey('genre.genre'), primary_key=True)


class Playlist(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), db.ForeignKey("user.username"))
    user = db.relationship("User", backref=db.backref("books", lazy="dynamic"))
    playlistName = db.Column(db.String(100))
    musicId = db.Column(db.Integer, db.ForeignKey("music.entryId"))
    music = db.relationship("Music", backref=db.backref("books", lazy="dynamic"))


def get_sample():
    return Music.query.all()


def get_details(id):
    return Music.query.get_or_404(id)


def get_author(id):
    return Author.query.get_or_404(id)


def get_playlist():
    return Playlist.query.all()


def get_genre():
    return Genre.query.all()


def get_genre_by_name(name):
    return Genre.query.filter(Genre.genre == name).first()


def recuperer_note(id, user):
    return db.session.query(Note).filter(Note.username == user, Note.musicId == id)


# def get_recherche_author(res):
#     return Author.query.filter(Author.name.like("%"+str(res)+"%")).all()

def get_recherche(res):
    if Music.query.filter(Music.title.like("%" + str(res) + "%")).all() is None:
        return Music.query.filter(Music.releaseYear == int(res)).all()
    else:
        return Music.query.filter(Music.title.like("%" + str(res) + "%")).all()


def get_if_like(name,musique):
    if db.session.query(Note).filter(Note.musicId == musique, Note.username == name).first() is None:
        return False
    else:
        return True
def get_like(name,musique):
    yes = db.session.query(Note).filter(Note.musicId == musique, Note.username == name).first()
    if yes == None :
        return None
    if yes.note is None:
        return None
    else:
        return yes.note


def get_music_by_genre(genre):
    if db.session.query(Link).filter(Link.genre_id == genre).all() is None:
        return None
    else : 
        return db.session.query(Link).filter(Link.genre_id == genre).all()

def get_music_by_author(id):
    return Music.query.filter(Music.author_id == id).all()


def get_music_by_id(id):
    return db.session.query(Music).filter(Music.entryId == id).all()


def get_id_by_music(name):
    return Music.query(Music.entryId).filter(Music.title == name).one()


def get_author_by_name(name):
    if Author.query.filter(Author.name == name).all():
        return Author.query.filter(Author.name == name).one()
    else:
        return None


def get_liste_author():
    return Author.query.all()


def get_playlist_by_user(user):
    return db.session.query(Playlist).filter(Playlist.username == user).all()


def get_playlist_by_name_user(name, user):
    return db.session.query(Playlist).filter(Playlist.playlistName == name, Playlist.username == user).all()


def get_music(name):
    return Music.query.filter(Music.title == name).all()


def get_playlist_by_id(id):
    return db.session.query(Playlist).filter(Playlist.Id == id).one()


@login_manager.user_loader
def load_user(username):
    return User.query.get(username)


def get_genre_by_genre(genre):
    if Genre.query.filter(Genre.genre == genre).all():
        return Genre.query.filter(Genre.genre == genre).one()
    else:
        return None

def get_playlist_by_id(id):
    return db.session.query(Playlist).filter(Playlist.Id == id).one()



# ===============================================================Recherche===============================================================

def recherche_album(res):
    return Music.query.filter(Music.title.contains(res)).order_by(Music.title.asc()).all()


def recherche_artiste(res):
    return Music.query.filter(Music.parent.contains(res)).order_by(Music.title.asc()).all()


def recherche_genre(recherche):
    return Music.query.filter(Genre.genre.contains(recherche)).order_by(Music.title.asc()).all()


def recherche_annee(recherche):
    return Music.query.filter(Music.releaseYear.contains(recherche)).order_by(Music.title.asc()).all()
