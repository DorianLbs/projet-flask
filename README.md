Projet FLASK -*-
Dorian LABASSE
Grégoire Bouchard

Pour lancer le projet il faut:

pip install -r requirements.txt 
flask loaddb myapp/extrait.yml
flask run

S'inscrire puis se connecter pour accéder au site.

Informations importantes :

Nous avons effectué avec succès :

-possibilité de se register/login/logout
-le détail au click+survol
-l'utilisation de bootstrap
-création d'un album/auteur/genre + playlist(mais l'affiche des playlists autres que la playlist "Like" n'a pas été effectuée par manque de temps)

-modification d'un auteur(en cliquant sur son nom dans détail (d'un album) puis en cliquant sur son nom dans le détail de ses oeuvres) et d'un genre
-possibilité de "like" un album et de l'"unlike" ==>like un album le fait s'ajouter à la playlist "Like" et l'"unlike" le retire de celle-ci
-recherche avancée effectuée fonctionnelle
-possibilité de noter un album
-possibilité de supprimer un album/genre
-possibilité d'éditer un album dans détails(détails=quand on click sur un album)
-détail album/auteur
-loaddb/syncdb
-on peut écouter les musiques via l'API Youtube avec un lecteur intégré


-La pagination a été réalisée mais par manque de temps pour la rendre esthétique
nous avons mis le lien du home avec celle-ci dans le footer pour que vous puissiez la voir.

-un utilisateur peut créer plusieurs playlists en les initialisant avec une musique par playlist mais par manque
de temps nous n'avons pas réalisé l'affichage de celles-ci et nous sommes concentrés sur l'affichage de la playlist "Like"
